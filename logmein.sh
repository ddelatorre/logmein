#!/usr/bin/expect --
# author:       Daniel De La Torre
# desc:         Handles passing in password for commands.
# dependencies: Run install.sh before use.

set pwfile          [lindex $argv 0];
set cmd             [lrange $argv 1 end];

# Setting a generic regex for the prompt is very trick especially considering 
# that we also have to handle the prompt of the host machine and what we log
# into when using ssh so for this reason logmein will not support a prompt with
# a colon. 
set prompt          "\[%#>\\$\] \?";
set user            [exec whoami];
set remove_key      0
set valid_args      false

if { $pwfile != "" } {
    set valid_args      [ exec sh -c "test -f $pwfile && file $pwfile | grep -i ascii && echo true || echo false" ]
}

if { $valid_args == "false" || $cmd == "" } {
    puts "Missing password file argument and/or command argument."
    puts " "
    puts "  logmein pw cmd"
    puts "     pw  = location of the file with password."
    puts "           The password must exist as plain text. "
    puts "     cmd = Command to execute. "
    puts "Example "
    puts "    logmein /home/$user/projx-password ssh -p 2222 root@192.168.2.63"
    exit 1;
} else {
    set f [open $pwfile r]
    set pw [read $f]
    close $f
}

if { [catch { set usr [exec echo $cmd | grep -Eo "\\w+@(\[0-9\]+\.)+\[0-9\]+" | cut -d "@" -f1] } result]} {
    set config_host [exec echo $cmd | cut -d " " -f2]
    set usr [exec ssh -G $config_host | grep "^user " | cut -d " " -f2]
}

if { [catch { set ip [exec echo $cmd | grep -Eo "\\w+@(\[0-9\]+\.)+\[0-9\]+" | cut -d "@" -f2] } result]} {
    set config_host [exec echo $cmd | cut -d " " -f2]
    set ip [exec ssh -G $config_host | grep "^hostname " | cut -d " " -f2]
}

if { [catch { set port [exec echo $cmd | grep -Eio "\\-p +\[\[:digit:\]\]+" | cut -d " " -f2]} result] } {
    set port 22
}

eval spawn $cmd

while {1} \
{
    expect \
    {
        "assword" { send "$pw\r"; break }
        "Are you sure you want to continue connecting (yes/no)?" { send "yes\r" }
        "Are you sure you want to continue connecting (yes/no/\\\[fingerprint\\\])?" { send "yes\r" }
        "ost key verification failed" { spawn ssh-keygen -f "/home/$user/.ssh/known_hosts" -R "\[$ip\]:$port"; set remove_key 1; eval spawn $cmd }
        
        # Handle the case where there is no password and we immediately fall into a shell prompt 
        # and also handle the case where we get a prompt from needing to remove the host key.
        -re $prompt { if { $remove_key == 1 } { set remove_key 0; send "\r" } else { interact; exit 0 } }
    }
}

expect \
{
    "ermission denied" { exit 1 }
    -re $prompt { interact; exit 0 }
}


