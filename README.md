# logmein

## Install

Run ./install.sh which will install expect interpreter and create a symbolic
link at /usr/local/bin/logmein. 

## Usage

    logmein pw cmd
         pw  = location of the file with password.
               The password must exist as plain text. 
         cmd = Command to execute. 
    Example 
        logmein /home/user/projx-password ssh -p 2222 root@192.168.2.63
